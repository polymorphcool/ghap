/*
 * common.h
 *
 *  Created on: Sep 29, 2020
 *      Author: frankiezafe
 */

#ifndef MODULES_GHAP_COMMON_H_
#define MODULES_GHAP_COMMON_H_

extern "C" {
#include <libavformat/version.h>
#include <libavcodec/version.h>
}

#if (LIBAVFORMAT_VERSION_MAJOR > 57 || (LIBAVFORMAT_VERSION_MAJOR == 57 && LIBAVFORMAT_VERSION_MINOR >= 41))
#define OFX_HAP_HAS_CODECPAR 1
#else
#define OFX_HAP_HAS_CODECPAR 0
#endif

#if (LIBAVCODEC_VERSION_MAJOR > 57 || (LIBAVCODEC_VERSION_MAJOR == 57 && LIBAVCODEC_VERSION_MINOR >= 24))
#define OFX_HAP_HAS_PACKET_ALLOC 1
#else
#define OFX_HAP_HAS_PACKET_ALLOC 0
#endif

#endif /* MODULES_GHAP_COMMON_H_ */
