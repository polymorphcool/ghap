# GHAP

HAP video decompressor module for [godot engine](http://godotengine.org)

## about HAP

"Fast as f**: HAP is a set of video codecs for macOS, Windows and Linux that performs decompression using a computer's graphics hardware, substantially reducing the CPU usage necessary to play video"

source: https://hap.video

## compilation

based on [ofxHapPlayer](https://github.com/bangnoise/ofxHapPlayer)

### linux

dependencies

`sudo apt-get install libsnappy-dev libswresample-dev libavcodec-dev libavformat-dev libdispatch-dev`


