/*
 * clock.h
 *
 *  Created on: Sep 29, 2020
 *      Author: frankiezafe
 */

#ifndef MODULES_GHAP_CLOCK_H_
#define MODULES_GHAP_CLOCK_H_

#include <cstdint>

namespace ghap {
class Clock {
public:

	enum class Mode {
		Once, Loop, Palindrome
	};
	enum class Direction {
		Forwards, Backwards
	};

	Clock();
	virtual ~Clock();

    void    syncAt(int64_t pos, int64_t t);
    int64_t getTime() const;
    int64_t getTimeAt(int64_t t) const;
    int64_t setTimeAt(int64_t t);
    void    setPausedAt(bool paused, int64_t t);
    bool    getPaused() const;
    Direction   getDirectionAt(int64_t t) const;
    float   getRate() const;
    void    setRateAt(float r, int64_t t);
    bool    getDone() const;
    void    rescale(int old, int next);
    int64_t period;
    Mode    mode;
private:
    int64_t _start;
    int64_t _time;
    bool    _paused;
    float   _rate;
};
}

#endif /* MODULES_GHAP_CLOCK_H_ */
