/*
 * clock.cpp
 *
 *  Created on: Sep 29, 2020
 *      Author: frankiezafe
 */

#include "clock.h"

extern "C" {
#include <libavutil/avutil.h>
}

namespace ghap {
static int64_t clockMod(int64_t k, int64_t n) {
	return ((k %= n) < 0) ? k + n : k;
}
}

ghap::Clock::Clock() :
		period(0),
		mode(Mode::Loop),
		_start(0),
		_time(-1),
		_paused(false),
		_rate(1.0) {
}

ghap::Clock::~Clock() {
}

void ghap::Clock::syncAt(int64_t pos, int64_t t) {
	_start = t - static_cast<int64_t>(pos / _rate);
	_time = pos;
}

int64_t ghap::Clock::getTime() const {
	return _time;
}

int64_t ghap::Clock::getTimeAt(int64_t t) const {
	t = static_cast<int64_t>((t - _start) * _rate);

	if (_paused) {
		return _time;
	} else if (mode == Mode::Once) {
		if (t > period) {
			return period;
		} else if (t < 0) {
			// Once backwards
			if (t < -period) {
				return 0;
			} else {
				return period + t;
			}
		} else {
			return t;
		}
	} else if (period == 0) {
		return 0;
	} else if (mode == Mode::Palindrome && clockMod((t / period), 2) == 1) {
		return period - clockMod(t, period) - 1;
	} else {
		return clockMod(t, period);
	}
}

int64_t ghap::Clock::setTimeAt(int64_t t) {
	return _time = getTimeAt(t);
}

void ghap::Clock::setPausedAt(bool p, int64_t t) {
	if (_paused != p) {
		if (p) {
			setTimeAt(t);
		} else {
			syncAt(_time, t);
		}
		_paused = p;
	}
}

bool ghap::Clock::getPaused() const {
	return _paused;
}

ghap::Clock::Direction ghap::Clock::getDirectionAt(int64_t t) const {
	if (_paused) {
		t = _time;
	} else {
		t = static_cast<int64_t>((t - _start) * _rate);
	}
	if (period == 0) {
		return ghap::Clock::Direction::Forwards;
	} else if (mode == Mode::Palindrome && clockMod((t / period), 2) == 1) {
		if (_rate > 0) {
			return ghap::Clock::Direction::Backwards;
		} else {
			return ghap::Clock::Direction::Forwards;
		}
	} else {
		if (_rate > 0) {
			return ghap::Clock::Direction::Forwards;
		} else {
			return ghap::Clock::Direction::Backwards;
		}
	}
}

float ghap::Clock::getRate() const {
	return _rate;
}

void ghap::Clock::setRateAt(float r, int64_t t) {
	_rate = r;
	syncAt(_time, t);
}

bool ghap::Clock::getDone() const {
	return (mode == ghap::Clock::Mode::Once && getTime() == period) ?
			true : false;
}

void ghap::Clock::rescale(int old, int next) {
	period = av_rescale_q(period, { 1, old }, { 1, next });
	_start = av_rescale_q(_start, { 1, old }, { 1, next });
	_time = av_rescale_q(_time, { 1, old }, { 1, next });
}
