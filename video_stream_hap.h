/*
 * video_stream_hap.h
 *
 *  Created on: Sep 28, 2020
 *      Author: frankiezafe
 */

#ifndef VIDEO_STREAM_HAP_H_
#define VIDEO_STREAM_HAP_H_

#include <iostream>
#include "core/io/resource_loader.h"
#include "core/os/file_access.h"
#include "core/os/semaphore.h"
#include "core/os/thread.h"
#include "core/ring_buffer.h"
#include "scene/resources/video_stream.h"
#include "servers/audio_server.h"

// hap specific
#include <libavformat/avformat.h>
#include <libavutil/time.h>
#include <libswresample/swresample.h>
#include "hap/source/hap.h"
#include "clock.h"

class VideoStreamPlaybackHap: public VideoStreamPlayback {

GDCLASS(VideoStreamPlaybackHap, VideoStreamPlayback);

private:

	Image::Format format;
	FileAccess *file;
	String file_name;

	Ref<ImageTexture> texture;

	AudioMixCallback mix_callback;
	void *mix_udata;
	int audio_track;

	bool playing;
	bool paused;
	bool loop;
	int loop_count;

public:

	virtual void play();
	virtual void stop();
	virtual bool is_playing() const;

	virtual void set_paused(bool p_paused);
	virtual bool is_paused() const;

	virtual void set_loop(bool p_enable);
	virtual bool has_loop() const;

	virtual float get_length() const;

	virtual String get_stream_name() const;

	virtual int get_loop_count() const;

	virtual float get_playback_position() const;
	virtual void seek(float p_time);

	void set_file(const String &p_file);

	virtual Ref<Texture> get_texture() const;
	virtual void update(float p_delta);

	virtual void set_mix_callback(AudioMixCallback p_callback,
			void *p_userdata);
	virtual int get_channels() const;
	virtual int get_mix_rate() const;

	virtual void set_audio_track(int p_idx);

	VideoStreamPlaybackHap();
	virtual ~VideoStreamPlaybackHap();

};

class VideoStreamHap: public VideoStream {

GDCLASS(VideoStreamHap, VideoStream);

private:
	String file;
	int audio_track;

protected:
	static void _bind_methods();

public:
	Ref<VideoStreamPlayback> instance_playback() {
		Ref<VideoStreamPlaybackHap> pb = memnew(VideoStreamPlaybackHap);
		pb->set_audio_track(audio_track);
		pb->set_file(file);
		return pb;
	}

	void set_file(const String &p_file) {
		file = p_file;
	}
	String get_file() {
		return file;
	}
	void set_audio_track(int p_track) {
		audio_track = p_track;
	}

	VideoStreamHap() {
		audio_track = 0;
	}

};

class ResourceFormatLoaderHap: public ResourceFormatLoader {
public:
	virtual RES load(const String &p_path, const String &p_original_path = "",
			Error *r_error = NULL);
	virtual void get_recognized_extensions(List<String> *p_extensions) const;
	virtual bool handles_type(const String &p_type) const;
	virtual String get_resource_type(const String &p_path) const;
};

#endif /*VIDEO_STREAM_HAP_H_ */
