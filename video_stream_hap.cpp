/*
 * video_stream_hap.cpp
 *
 *  Created on: Sep 28, 2020
 *      Author: frankiezafe
 */

#include "video_stream_hap.h"

VideoStreamPlaybackHap::VideoStreamPlaybackHap() {

	file = NULL;
	playing = false;
	paused = false;
	loop = false;
	loop_count = 0;

	texture = Ref<ImageTexture>(memnew(ImageTexture));

	mix_callback = NULL;
	mix_udata = NULL;
	audio_track = 0;

}

VideoStreamPlaybackHap::~VideoStreamPlaybackHap() {
	// TODO Auto-generated destructor stub
}

void VideoStreamPlaybackHap::play() {
	std::cout << "VideoStreamPlaybackHap::play" << std::endl;
}

void VideoStreamPlaybackHap::stop() {
	std::cout << "VideoStreamPlaybackHap::stop" << std::endl;
}

bool VideoStreamPlaybackHap::is_playing() const {
	return playing;
}

void VideoStreamPlaybackHap::set_paused(bool p_paused) {
	std::cout << "VideoStreamPlaybackHap::set_paused: " << p_paused
			<< std::endl;
}

bool VideoStreamPlaybackHap::is_paused() const {
	return paused;
}

void VideoStreamPlaybackHap::set_loop(bool p_enable) {
	loop = p_enable;
}

bool VideoStreamPlaybackHap::has_loop() const {
	return loop;
}

float VideoStreamPlaybackHap::get_length() const {
	return 0.f;
}

String VideoStreamPlaybackHap::get_stream_name() const {
	return "";
}

int VideoStreamPlaybackHap::get_loop_count() const {
	return loop_count;
}

float VideoStreamPlaybackHap::get_playback_position() const {
	return 0.f;
}

void VideoStreamPlaybackHap::seek(float p_time) {
	std::cout << "VideoStreamPlaybackHap::seek: " << p_time << std::endl;
}

void VideoStreamPlaybackHap::set_file(const String &p_file) {

	ghap::Clock c;
	std::cout << c.getTime() << std::endl;

	ERR_FAIL_COND(playing);

	file_name = p_file;
	if (file) {
		memdelete(file);
	}
	file = FileAccess::open(p_file, FileAccess::READ);
	ERR_FAIL_COND_MSG(!file, "Cannot open file '" + p_file + "'.");

	std::cout << "VideoStreamPlaybackHap::set_file: " << p_file.c_str()
			<< std::endl;

	std::cout << c.getTime() << std::endl;

}

Ref<Texture> VideoStreamPlaybackHap::get_texture() const {
	return texture;
}

void VideoStreamPlaybackHap::update(float p_delta) {
	std::cout << "VideoStreamPlaybackHap::update: " << p_delta << std::endl;
}

void VideoStreamPlaybackHap::set_mix_callback(AudioMixCallback p_callback,
		void *p_userdata) {
}

int VideoStreamPlaybackHap::get_channels() const {
	return 0;
}

int VideoStreamPlaybackHap::get_mix_rate() const {
	return 0;
}

void VideoStreamPlaybackHap::set_audio_track(int p_idx) {
	audio_track = p_idx;
}

////////////

void VideoStreamHap::_bind_methods() {

	ClassDB::bind_method(D_METHOD("set_file", "file"),
			&VideoStreamHap::set_file);
	ClassDB::bind_method(D_METHOD("get_file"), &VideoStreamHap::get_file);

	ADD_PROPERTY(
			PropertyInfo(Variant::STRING, "file", PROPERTY_HINT_NONE, "",
					PROPERTY_USAGE_NOEDITOR | PROPERTY_USAGE_INTERNAL),
			"set_file", "get_file");

}

////////////

RES ResourceFormatLoaderHap::load(const String &p_path,
		const String &p_original_path, Error *r_error) {

	FileAccess *f = FileAccess::open(p_path, FileAccess::READ);
	if (!f) {
		if (r_error) {
			*r_error = ERR_CANT_OPEN;
		}
		return RES();
	}

	VideoStreamHap *stream = memnew(VideoStreamHap);
	stream->set_file(p_path);

	Ref<VideoStreamHap> hap_stream = Ref<VideoStreamHap>(stream);

	if (r_error) {
		*r_error = OK;
	}

	f->close();
	memdelete(f);
	return hap_stream;
}

void ResourceFormatLoaderHap::get_recognized_extensions(
		List<String> *p_extensions) const {
	p_extensions->push_back("mov");
}

bool ResourceFormatLoaderHap::handles_type(const String &p_type) const {
	return ClassDB::is_parent_class(p_type, "VideoStream");
}

String ResourceFormatLoaderHap::get_resource_type(const String &p_path) const {

	String el = p_path.get_extension().to_lower();
	if (el == "mov")
		return "VideoStreamHap";
	return "";
}
